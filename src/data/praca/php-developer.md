---
isActive: true
title: 'PHP Developer'
description: 'Miejsce pracy: Bydgoszcz'
---

<h4>Zatrudniona osoba będzie odpowiedzialna za:</h4>

- Udział w pełnym cyklu tworzenia projektu (zebranie wymagań, tworzenie, implementacja i dostarczenie dokumentacji technicznej)
- Pracę w zespołach Scrum'owych
- Programowanie zgodnie z zasadami clean architecture i SOLID
- Łączenie wielu narzędzi: PHP, RabbitMQ, Mysql
- Przygotowywanie testów PHPUnit, Codeception
- Tworzenie kodu nastawionego na wysoką wydajność i niezawodność
- Realizację różnorodnych zadań
- Wykorzystanie GIT, Elasticsearch

<h4>Od kandydatów oczekujemy:</h4>

- Biegłej umiejętności programowania PHP
- Praktycznej znajomości programowania obiektowego PHP
- Bardzo dobrej wiedzy z zakresu relacyjnych baz danych (MySQL)
- Umiejętności posługiwania się systemem kontroli wersji GIT

<h4>Mile widziane:</h4>

- Znajomość Symfony/Laravel Framework
- Znajomość RabbitMQ, Elasticsearch
- Znajomości dobrych praktyk SOLID
- Tworzenia testów PHPUnit
- Znajomość Javascript

<h4>Oraz:</h4>

- Zaangażowania
- Kreatywności
- Umiejętności pracy w zespole

<h4>Oferujemy:</h4>

- Stałą pracę oraz możliwość podnoszenia swoich kwalifikacji zawodowych
- Możliwość uzyskania certyfikatu Zend PHP Certification
- Pracę w doświadczonym zespole, przy interesujących projektach umożliwiających rozwój i gromadzenie doświadczenia
- Możliwość bezpłatnego uczestnictwa w konferencjach branżowych
- Premie uznaniowe
- Zajęcia z j. angielskiego w godzinach pracy
- Pakiet szkoleń
- Możliwość udziału w zajęciach sportowych po pracy - siatkówka, piłka nożna, kręgle
- Kartę Multisport
- Wyjazdy i imprezy integracyjne
- Grupowe ubezpieczenie na życie

<br/>
CV prosimy przesyłać na adres: <a href="mailto:praca@makromedia.pl">praca@makromedia.pl</a>
