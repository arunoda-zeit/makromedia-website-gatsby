import { colors } from './colors'

const mediaBreakpoints = {
  xs: 0, // em
  sm: 32, // em
  md: 54, // em
  lg: 85, // em
}

export const theme = {
  colors,
  font: {
    family: {
      primary: `"Raleway", sans-serif`,
      secondary: `"Open Sans", sans-serif`,
      accent: `"Cardo", Georgia, 'Times New Roman', serif`,
    },
    weight: {
      thin: 200,
      light: 300,
      regular: 400,
      semibold: 600,
      bold: 700,
    },
    size: {
      base: 14,
    },
  },
  media: {
    mobile: `(min-width: ${mediaBreakpoints.sm}em)`,
    tablet: `(min-width: ${mediaBreakpoints.md}em)`,
    desktop: `(min-width: ${mediaBreakpoints.lg}em)`,
  },
  flexboxgrid: {
    gridSize: 12, // columns
    gutterWidth: 1, // rem
    outerMargin: 1, // rem
    mediaQuery: 'only screen',
    container: {
      sm: mediaBreakpoints.sm, // rem
      md: mediaBreakpoints.md - 2, // rem
      lg: mediaBreakpoints.lg - 18, // rem
    },
    breakpoints: mediaBreakpoints,
  },
}
