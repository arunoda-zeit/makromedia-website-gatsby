import styled from 'styled-components'

const AppSeparator = styled.div`
  height: 2px;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.nav};
  margin: 30px 0px;
`

export default AppSeparator
