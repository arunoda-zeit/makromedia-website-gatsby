import { graphql, StaticQuery } from 'gatsby'
// import Img from 'gatsby-image'
// import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'
import { Grid, Col, Row } from 'react-styled-flexboxgrid'
import ContactFormCard from '../ContactForm/ContactFormCard'
import BackgroundImg from '../BackgroundImg'
import AppContainer from '../AppContainer'

const BackgroundSection = props => (
  <StaticQuery
    query={componentQuery}
    render={data => {
      return (
        <>
          <BackgroundImg
            className={props.className}
            fluid={data.bgImage.childImageSharp.fluid}
            height="100vh"
            position="center top"
          />
          <StyledContainer>
            <StyledGrid fluid={true}>
              <Row>
                <Col xs={12} md={8}>
                  <BannerText className="banner-text text-center">
                    <h1>
                      POSTAW NA WIDOCZNOŚĆ
                      <br />
                      DAJ SIĘ ZNALEŹĆ SWOIM KLIENTOM
                    </h1>
                    <p>
                      Spraw by Twoją stronę w Internecie odwiedzało więcej
                      klientów
                      <br />
                      poszukujących produktów lub usług, które oferujesz.
                    </p>
                    {/* <!-- <a href="#" className="btn btn-large">Zobacz ofertę</a> --> */}
                  </BannerText>
                </Col>
                <Col xs={12} md={4}>
                  <ContactFormCard />
                </Col>
              </Row>
            </StyledGrid>
          </StyledContainer>
        </>
      )
    }}
  />
)

const HomeBannerSection = styled(BackgroundSection)`
  margin: 0 auto;
  padding-top: 100px;
`

const StyledContainer = styled(AppContainer)`
  height: 100%;
  margin: 0 auto;

  @media ${({ theme }) => theme.media.tablet} {
    height: calc(100vh - 100px);
    width: 100%;
    max-width: 100%;
  }

  @media ${({ theme }) => theme.media.desktop} {
    width: 1170px;
    max-width: 1170px;
  }
`

const StyledGrid = styled(Grid)`
  padding: 0;

  @media ${({ theme }) => theme.media.tablet} {
    padding: 0 2rem;
  }
`

const BannerText = styled.div`
  padding: 0;
  margin: 0 3%;
  height: calc(100vh - 60px);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  user-select: none;

  @media ${({ theme }) => theme.media.tablet} {
    height: calc(100vh - 100px);
    margin: 0;
  }

  @media only screen and (min-height: 1000px) {
    padding-top: 100px;
    height: 1000px;
  }

  h1 {
    color: ${({ theme }) => theme.colors.white};
    font-size: 30pt;
    font-family: ${({ theme }) => theme.font.family.secondary};
    font-weight: ${({ theme }) => theme.font.weight.bold};
    text-transform: uppercase;
  }

  p {
    color: ${({ theme }) => theme.colors.white};
    font-size: 22px;
    font-weight: ${({ theme }) => theme.font.weight.light};
    line-height: 1.5;
  }
`

const componentQuery = graphql`
  query {
    bgImage: file(relativePath: { eq: "banner.jpg" }) {
      childImageSharp {
        fluid(quality: 98) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default HomeBannerSection
