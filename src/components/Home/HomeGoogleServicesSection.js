import React from 'react'
import { graphql, StaticQuery } from 'gatsby'
import styled from 'styled-components'
import { Col, Row } from 'react-styled-flexboxgrid'
import AppSeparator from '../AppSeparator'
import AppContainer from '../AppContainer'

const HomeGoogleServicesSection = () => (
  <StaticQuery
    query={componentQuery}
    render={data => (
      <Section>
        <AppContainer>
          <AppSeparator />
          <StyledSectionTitle>
            Wsparci wiedzą i doświadczeniem Google
          </StyledSectionTitle>
          <h3>
            Praktyczne podejście do budżetu marketingowego i sprawdzone
            narzędzia
          </h3>
          <Row>
            <Col xs={12} md={3}>
              <Img src={data.logoAnatytics.publicURL} alt="google analytics" />
            </Col>
            <Col xs={12} md={3}>
              <Img src={data.logoAdwords.publicURL} alt="google adwords" />
            </Col>
            <Col xs={12} md={3}>
              <Img src={data.logoGoogleMaps.publicURL} alt="google analytics" />
            </Col>
            <Col xs={12} md={3}>
              <Img src={data.logoLongTail.publicURL} alt="long tail platinum" />
            </Col>
          </Row>
        </AppContainer>
      </Section>
    )}
  />
)

const Section = styled.section`
  text-align: center;

  h2,
  h3 {
    font-family: ${({ theme }) => theme.font.family.primary};
    color: ${({ theme }) => theme.colors.fontColor};
    text-align: center;
  }

  h3 {
    font-size: 18px;
    font-weight: 300;
  }
`

const StyledSectionTitle = styled.h2`
  font-weight: ${({ theme }) => theme.font.weight.regular};
  font-size: 28px;
  margin-top: 60px;
`

const Img = styled.img`
  width: 100%;
  max-width: 300px;
  margin: 10px auto;

  @media ${({ theme }) => theme.media.tablet} {
    max-width: 100%;
    margin: 60px auto;
  }
`

const componentQuery = graphql`
  query {
    logoAnatytics: file(relativePath: { eq: "logo_analytics.png" }) {
      publicURL
    }
    logoAdwords: file(relativePath: { eq: "logo_adwords.png" }) {
      publicURL
    }
    logoGoogleMaps: file(relativePath: { eq: "logo_google_maps.png" }) {
      publicURL
    }
    logoLongTail: file(relativePath: { eq: "Long-Tail-Platinium.png" }) {
      publicURL
    }
  }
`

export default HomeGoogleServicesSection
