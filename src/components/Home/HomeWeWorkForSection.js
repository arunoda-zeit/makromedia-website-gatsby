import React from 'react'
import styled from 'styled-components'
import { Row, Col } from 'react-styled-flexboxgrid'
import SectionTitle from '../SectionTitle'
import AppContainer from '../AppContainer'
import InternetIcon from '../../images/icons/internet.svg'
import ShopIcon from '../../images/icons/shop.svg'
import StartupIcon from '../../images/icons/startup.svg'

const HomeWeWorkForSection = () => (
  <section>
    <SectionTitle>Dla kogo pracujemy?</SectionTitle>
    <AppContainer>
      <Row>
        <Column xs={12} md={4}>
          <Box color="green">
            <StyledInternetIcon />
            <h3>Firmy ogólnopolskie</h3>
            <p>
              Pakiet usług premium, które budują zasięg, wizerunek i sprzedaż
              firmy na rynku polskim i zagranicznym
            </p>
          </Box>
        </Column>
        <Column xs={12} md={4}>
          <Box color="orange">
            <StyledShopIcon />
            <h3>Firmy lokalne</h3>
            <p>Atrakcyjne kosztowo rozwiązania dla małych i średnich firm</p>
          </Box>
        </Column>
        <Column xs={12} md={4}>
          <Box color="purple">
            <StyledStartupIcon />
            <h3>Nowe firmy</h3>
            <p>Nowa strona z pozycjonowaniem w dobrej cenie</p>
          </Box>
        </Column>
      </Row>
    </AppContainer>
  </section>
)

const Column = styled(Col)`
  text-align: center;
`

const Box = styled.div.attrs(({ color }) => ({
  boxColor: color || '#bcbdc1',
}))`
  padding: 10px;
  background-color: ${({ theme }) => theme.colors.white};
  max-width: 350px;
  height: 380px;
  margin: 15px auto;
  cursor: pointer;
  border: 2px solid ${({ boxColor, theme }) => theme.colors[boxColor]};

  * {
    transition-duration: 0.3s;
    transition-property: all;
  }

  h3 {
    font-family: ${({ theme }) => theme.font.family.secondary};
    font-size: 20pt;
    font-weight: 200;
    color: ${({ theme }) => theme.colors.fontColor};
  }

  p {
    font-size: 12pt;
  }

  h3,
  p {
    color: ${({ boxColor, theme }) => theme.colors[boxColor]};
  }

  svg {
    path,
    circle {
      fill: ${({ boxColor, theme }) => theme.colors[boxColor]};
    }
  }

  @media ${({ theme }) => theme.media.tablet} {
    border: 2px solid #bcbdc1;
    color: ${({ theme }) => theme.colors.fontColor};

    h3,
    p {
      color: ${({ theme }) => theme.colors.fontColor};
    }

    svg {
      path,
      circle {
        fill: #bcbdc1;
      }
    }

    &:hover {
      border: 2px solid ${({ boxColor, theme }) => theme.colors[boxColor]};

      h3,
      p {
        color: ${({ boxColor, theme }) => theme.colors[boxColor]};
      }

      svg {
        path,
        circle {
          fill: ${({ boxColor, theme }) => theme.colors[boxColor]};
        }
      }
    }
  }
  @media ${({ theme }) => theme.media.desktop} {
    margin: 15px 15px;
  }
`

const styledIcon = function(icon) {
  return styled(icon)`
    margin: 15% 25%;
    margin-bottom: 5%;
    width: auto;
    height: auto;
  `
}

const StyledInternetIcon = styledIcon(InternetIcon),
  StyledShopIcon = styledIcon(ShopIcon),
  StyledStartupIcon = styledIcon(StartupIcon)

export default HomeWeWorkForSection
