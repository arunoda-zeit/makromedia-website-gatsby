import React from 'react'
import styled from 'styled-components'
import SectionTitle from '../SectionTitle'
import AppContainer from '../AppContainer'
import { FaArrowRight } from 'react-icons/fa'

const HomeBenefitsSection = () => (
  <Section>
    <SectionTitle>Korzyści z pozycjonowania strony internetowej</SectionTitle>
    <AppContainer>
      <List>
        <ListItem>
          <Icon />
          najkorzystniejsza cenowo inwestycja w promocję Twojej strony
        </ListItem>
        <ListItem>
          <Icon />
          wzrost ilości konwersji
        </ListItem>
        <ListItem>
          <Icon />
          wzrost sprzedaży
        </ListItem>
        <ListItem>
          <Icon />
          wysokie pozycje fraz w wynikach wyszukiwania
        </ListItem>
        <ListItem>
          <Icon />
          rosnąca liczba trafnych odwiedzin na stronie
        </ListItem>
      </List>
    </AppContainer>
  </Section>
)

const Section = styled.section`
  padding-top: 40px;
  padding-bottom: 100px;
  background-color: #f1f9fb;
  clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3vw), 0 100%);
`

const List = styled.ul`
  margin-top: 30px;
`

const ListItem = styled.li`
  font-family: ${({ theme }) => theme.font.family.secondary};
  font-weight: ${({ theme }) => theme.font.weight.thin};
  font-size: 14pt;
  line-height: 24pt;
  vertical-align: middle;
  max-width: 600px;
  margin: 0 auto;
`
const Icon = styled(FaArrowRight)`
  margin-right: 12px;
  font-size: 15pt;
  display: inline-block;
  vertical-align: middle;
`
export default HomeBenefitsSection
