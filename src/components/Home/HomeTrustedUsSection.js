import React from 'react'
import styled from 'styled-components'
import { graphql, StaticQuery } from 'gatsby'
import { Col, Row } from 'react-styled-flexboxgrid'
import AppContainer from '../AppContainer'
import SectionTitle from '../SectionTitle'
import AppSeparator from '../AppSeparator'

const HomeTrustedUsSection = () => (
  <StaticQuery
    query={componentQuery}
    render={data => (
      <Section>
        <AppContainer>
          <AppSeparator />
          <SectionTitle>Zaufali Nam</SectionTitle>

          <Row>
            <Column xs={12} md={3}>
              <Logo alt="logo" src={data.logoPlaceholder.publicURL} />
              <p>Branża</p>
            </Column>
            <Column xs={12} md={3}>
              <Logo alt="logo" src={data.logoPlaceholder.publicURL} />
              <p>Branża</p>
            </Column>
            <Column xs={12} md={3}>
              <Logo alt="logo" src={data.logoPlaceholder.publicURL} />
              <p>Branża</p>
            </Column>
            <Column xs={12} md={3}>
              <Logo alt="logo" src={data.logoPlaceholder.publicURL} />
              <p>Branża</p>
            </Column>
          </Row>
          <Row>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
          </Row>
          <Row>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
            <Column xs={6} md={3}>
              <p>Lorem Impsum</p>
            </Column>
          </Row>
        </AppContainer>
      </Section>
    )}
  />
)

const Section = styled.section`
  padding: 0 3%;

  ${Row} {
    p {
      font-size: 26px;
      color: ${({ theme }) => theme.colors.fontColor};
      font-weight: 100;
    }
  }
`

const Column = styled(Col)`
  text-align: center;
  padding: 24px;

  @media ${({ theme }) => theme.media.tablet} {
    padding: 18px;
  }
`

const Logo = styled.img`
  width: 90%;
  max-width: 223px;
  margin-bottom: 10px;
`

const componentQuery = graphql`
  query {
    logoPlaceholder: file(relativePath: { eq: "logo_placeholder.png" }) {
      publicURL
    }
  }
`

export default HomeTrustedUsSection
