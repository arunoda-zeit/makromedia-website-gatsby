import React from 'react'
// import Media from 'react-media'
import styled from 'styled-components'
import { Col, Row } from 'react-styled-flexboxgrid'
import PictureWoman from '../../images/woman.jpg'
import IconPicture from '../../images/icons/picture.svg'
import IconBarChart from '../../images/icons/bar-chart.svg'
import IconNewFile from '../../images/icons/new-file.svg'
import IconGiveMoney from '../../images/icons/give-money.svg'
import AppContainer from '../AppContainer'
import SectionTitle from '../SectionTitle'

const HomeWhatWeDoSection = ({ data }) => (
  <StyledSection className="section what-we-do">
    <SectionTitle className="title text-center">
      Czym się zajmujemy
    </SectionTitle>
    <AppContainer>
      <Row>
        <Col xs={0} md={5} />
        <Col xs={12} md={7}>
          <BoxContainer>
            <ColorBox
              title="Publikacja"
              description="Krótki opis tego czym się firma zajmuje"
            >
              <IconPicture />
            </ColorBox>
            <ColorBox
              title="Analityka"
              description="Krótki opis tego czym się firma zajmuje"
            >
              <IconBarChart />
            </ColorBox>
            <ColorBox
              title="Pozycjonowanie"
              description="Krótki opis tego czym się firma zajmuje"
            >
              <IconNewFile />
            </ColorBox>

            <ColorBox
              title="Promocja"
              description="Krótki opis tego czym się firma zajmuje"
            >
              <IconGiveMoney />
            </ColorBox>
          </BoxContainer>
        </Col>
      </Row>
    </AppContainer>
  </StyledSection>
)

const ColorBox = props => (
  <StyledBox>
    <div className="box-content">
      {props.children}
      <p className="header">{props.title}</p>
      <p className="description">{props.description}</p>
    </div>
  </StyledBox>
)

const StyledSection = styled.section`
  margin-bottom: 40px;

  @media ${({ theme }) => theme.media.tablet} {
    background-image: url(${PictureWoman});
    background-repeat: no-repeat;
    background-size: contain;
    background-position-x: left;
    background-position-y: bottom;
    height: 100%;
    min-height: 430px;
    padding: 0;
    margin-top: 30px;
    margin-bottom: 0;
  }
`

const BoxContainer = styled.div`
  margin: 0 5%;
  display: flex;
  flex-wrap: wrap;
  max-width: 500px;

  @media ${({ theme }) => theme.media.mobile} {
    margin: 20px auto 50px;
  }
`

const StyledBox = styled.div`
  position: relative;
  flex-basis: calc(50% - 10px);
  margin: 5px;
  box-sizing: border-box;
  transition: opacity 0.2s;

  ::before {
    content: '';
    display: block;
    padding-top: 100%;
  }

  :hover {
    opacity: 0.9;
  }

  &:nth-child(1) div {
    background-color: ${({ theme }) => theme.colors.green};
  }
  &:nth-child(2) div {
    background-color: ${({ theme }) => theme.colors.orange};
  }
  &:nth-child(3) div {
    background-color: ${({ theme }) => theme.colors.purple};
  }
  &:nth-child(4) div {
    background-color: ${({ theme }) => theme.colors.burgundy};
  }

  .box-content {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    color: ${({ theme }) => theme.colors.white};
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;

    svg {
      margin-top: 5%;
      min-width: 50px;
      width: 30%;
    }

    p.header {
      margin: 0 3% 0;
      text-transform: uppercase;
      font-family: ${({ theme }) => theme.font.family.primary};
      font-weight: 400;
      font-size: 11pt;
      color: ${({ theme }) => theme.colors.white};

      @media ${({ theme }) => theme.media.mobile} {
        font-size: 13pt;
      }
    }

    p.description {
      font-family: ${({ theme }) => theme.font.family.secondary};
      font-size: 9pt;
      color: ${({ theme }) => theme.colors.white};
      text-align: center;
      margin: 3% 3% 5%;

      @media ${({ theme }) => theme.media.mobile} {
        font-size: 10pt;
      }
    }
  }
`
// const StyledContainer = styled(AppContainer)`
//   @media ${({ theme }) => theme.media.tablet} {
//     width: 100%;
//     max-width: 100%;
//   }

//   @media ${({ theme }) => theme.media.desktop} {
//     width: 1170px;
//     max-width: 1170px;
//   }
// `

export default HomeWhatWeDoSection
