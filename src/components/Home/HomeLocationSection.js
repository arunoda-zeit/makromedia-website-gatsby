import React from 'react'
import styled from 'styled-components'
import AppGoogleMap from '../AppGoogleMap'
import SectionTitle from '../SectionTitle'

const HomeLocationSection = () => (
  <Section>
    <SectionTitle>Lokalizacja</SectionTitle>
    <MapWrapper>
      <AppGoogleMap />
    </MapWrapper>
  </Section>
)

const Section = styled.section`
  margin-top: 50px;
`

const MapWrapper = styled.div`
  position: relative;
  overflow: initial;
  width: 100%;
  height: 350px;
  margin: 0;
  padding: 0;
  z-index: 1;
`

export default HomeLocationSection
