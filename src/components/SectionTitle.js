import styled from 'styled-components'

const SectionTitle = styled.h2`
  color: ${({ theme }) => theme.colors.title};
  font-size: 16pt;
  font-weight: ${({ theme }) => theme.font.weight.bold};
  text-align: center;
  margin-top: 30px;

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 20pt;
  }

  :after {
    content: '';
    display: block;
    height: 15px;
  }
`

export default SectionTitle
