// import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

class AppPageTitle extends React.Component {
  render() {
    return <Header>{this.props.title}</Header>
  }
}

const Header = styled.h2`
  opacity: 0.4;
  margin: 0 auto;

  ::after {
    content: '';
    display: block;
    padding: 10px;
    border-bottom: 1px solid ${({ theme }) => theme.colors.black};
  }

  @media ${({ theme }) => theme.media.tablet} {
    margin: 10px 0 0;
  }
`

export default AppPageTitle
