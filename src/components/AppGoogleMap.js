import React from 'react'
import { compose, withProps } from 'recompose'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps'
import config from '../config/variables'
import blueMapStyles from '../config/googleMapStyle.json'
import markerIcon from '../images/marker.png'

const target = { lat: 53.1331914, lng: 17.993677 }

const StyledMap = compose(
  withProps({
    center: target,
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${
      config.googleMapsApiKey
    }`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    {...props}
    defaultZoom={14}
    defaultCenter={props.center}
    defaultOptions={{
      styles: blueMapStyles,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false,
    }}
  >
    <Marker position={target} icon={markerIcon} />
  </GoogleMap>
))

export default StyledMap
