// import PropTypes from 'prop-types'
import styled from 'styled-components'

const ContactFormInput = styled.input`
  border: 2px solid #999;
  border-radius: 20px;
  width: 100%;
  height: 40px;
  color: #444;
  font-weight: 200;
  line-height: 25px;
  font-size: 20px;
  padding: 1px 15px;
  margin: 13px auto;
  outline: none;

  &:focus {
    border-color: ${({ theme }) => theme.colors.nav};
  }
`

export default ContactFormInput
