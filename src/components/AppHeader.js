import { Link, graphql, StaticQuery } from 'gatsby'
// import PropTypes from 'prop-types'
import React from 'react'
import styled, { css } from 'styled-components'
import { Grid } from 'react-styled-flexboxgrid'
import debounce from 'lodash/debounce'

class AppHeader extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isMenuExpanded: false,
      headerScrolled: false,
    }
    this.toggleMenu = this.toggleMenu.bind(this)
    this.scrollHandler = this.scrollHandler.bind(this)
    this.debouncedScrollHandler = this.debouncedScrollHandler.bind(this)
  }

  componentDidMount() {
    window.addEventListener('scroll', this.debouncedScrollHandler)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.debouncedScrollHandler)
  }

  debouncedScrollHandler = debounce(this.scrollHandler, 50)

  scrollHandler(event, scope) {
    if (window.scrollY >= 50) {
      this.setState((prevState, props) => ({
        headerScrolled: true,
      }))
    } else {
      this.setState((prevState, props) => ({
        headerScrolled: false,
      }))
    }
  }

  toggleMenu() {
    this.setState((prevState, props) => ({
      isMenuExpanded: !prevState.isMenuExpanded,
    }))
  }

  render() {
    return (
      <StaticQuery
        query={componentQuery}
        render={data => (
          <>
            <Header headerScrolled={this.state.headerScrolled}>
              <Content>
                <Link to="/">
                  <Logo
                    src={data.logoFile.publicURL}
                    alt="logo firmy Makromedia"
                  />
                </Link>
                <Navigation isMenuExpanded={this.state.isMenuExpanded}>
                  <NavigationList>
                    <NavigationListItem>
                      <Link
                        to="/o-firmie"
                        activeStyle={{ textDecoration: `underline` }}
                      >
                        O firmie
                      </Link>
                    </NavigationListItem>
                    <NavigationListItem>
                      <Link
                        to="/oferta"
                        activeStyle={{ textDecoration: `underline` }}
                      >
                        Oferta
                      </Link>
                    </NavigationListItem>
                    <NavigationListItem>
                      <Link
                        to="/kontakt"
                        activeStyle={{ textDecoration: `underline` }}
                      >
                        Kontakt
                      </Link>
                    </NavigationListItem>
                    <NavigationListItem>
                      <Link
                        to="/praca"
                        activeStyle={{ textDecoration: `underline` }}
                      >
                        Praca
                      </Link>
                    </NavigationListItem>
                  </NavigationList>
                </Navigation>
                <MenuButton
                  onClick={this.toggleMenu}
                  isMenuExpanded={this.state.isMenuExpanded}
                >
                  Menu
                  <HamburgerIcon />
                </MenuButton>
              </Content>
            </Header>
            <Spacer />
          </>
        )}
      />
    )
  }
}

const Logo = styled.img`
  height: 58px;
  transition: height 0.3s;
  margin-left: 7px;

  @media ${({ theme }) => theme.media.tablet} {
    height: 80px;
    margin-left: -7px;
  }
`

const Header = styled.header`
  position: fixed;
  width: 100%;
  height: 60px;
  max-height: 100px;
  top: 0;
  z-index: 999;
  background-color: ${({ theme }) => theme.colors.white};
  box-shadow: transparent 0 0 9px;
  transition: all 0.3s;

  ${({ headerScrolled }) =>
    headerScrolled &&
    css`
      box-shadow: rgba(0, 0, 0, 0.13) 0 0 9px;
    `}

  @media ${({ theme }) => theme.media.tablet} {
    height: 100px;

    ${({ headerScrolled }) =>
      headerScrolled &&
      css`
        height: 80px;
        box-shadow: rgba(0, 0, 0, 0.13) 0 0 9px;
      `}
  }
`

const Spacer = styled.div`
  display: block;
  height: 60px;
  transition: all 0.3s;

  @media ${({ theme }) => theme.media.tablet} {
    height: 100px;
  }

  ${({ headerScrolled }) =>
    headerScrolled &&
    css`
      height: 80px;
    `}
`

const Content = styled(Grid)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  height: 100%;
  background-color: ${({ theme }) => theme.colors.white};
  border-bottom: none;
  transition: height 0.3s;
  padding: 0;

  a:hover,
  a:focus {
    filter: none;
  }
`

const Navigation = styled.nav`
  position: fixed;
  background-color: ${({ theme }) => theme.colors.black};
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 99999;
  visibility: hidden;
  opacity: 0;
  transition: opacity 0.5s, visibility 0s 0.5s;

  @media ${({ theme }) => theme.media.tablet} {
    position: initial;
    background-color: transparent;
    width: auto;
    height: auto;
    top: auto;
    left: auto;
    z-index: initial;
    opacity: 0.9;
    visibility: visible;
  }

  ${({ isMenuExpanded }) =>
    isMenuExpanded &&
    css`
      opacity: 0.9;
      visibility: visible;
      transition: opacity 0.5s;
    `}
`

const NavigationList = styled.ul`
  position: relative;
  top: 45%;
  transform: translateY(-45%);

  @media ${({ theme }) => theme.media.tablet} {
    top: auto;
    transform: translateY(0);
  }
`

const NavigationListItem = styled.div`
  display: block;
  margin-bottom: 20px;

  a {
    display: block;
    font-size: 25px;
    margin: 0;
    text-align: center;
    color: ${({ theme }) => theme.colors.nav};
    text-transform: uppercase;
    font-weight: ${({ theme }) => theme.font.weight.bold};
    transition: transform 0.1s;

    &:hover,
    &.active {
      text-decoration: underline;
      color: ${({ theme }) => theme.colors.white};

      @media ${({ theme }) => theme.media.tablet} {
        opacity: 0.8;
        color: inherit;
      }
    }

    &:hover {
      transform: scale(1.2);
    }
  }

  @media ${({ theme }) => theme.media.tablet} {
    display: inline-block;
    margin-bottom: 0;

    a {
      font-size: ${({ theme }) => theme.font.size.base}px;
      margin-left: 40px;

      &:hover {
        transform: none;
      }
    }
  }
`

const MenuButton = styled.a`
  display: block;
  height: 44px;
  overflow: hidden;
  position: fixed;
  right: 2.5%;
  text-indent: 100%;
  top: 8px;
  white-space: nowrap;
  width: 44px;
  z-index: 99999;
  transition: all 0.3s;
  cursor: pointer;

  &:before,
  &:after {
    border-radius: 50%;
    content: '';
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    transform: translateZ(0);
    backface-visibility: hidden;
    transition-property: transform;
  }
  &:before {
    background-color: ${({ theme }) => theme.colors.brand};
    transform: scale(1);
    transition-duration: 0.3s;
  }

  &:after {
    background-color: ${({ theme }) => theme.colors.brand};
    transform: scale(0);
    transition-duration: 0s;
  }

  ${({ isMenuExpanded }) =>
    isMenuExpanded &&
    css`
      &:before {
        transform: scale(0);
      }
      &:after {
        transform: scale(1);
      }
      span {
        background-color: rgba(255, 255, 255, 0);
        &:before,
        &:after {
          background-color: ${({ theme }) => theme.colors.white};
        }
        &:before {
          transform: translateY(0) rotate(45deg);
        }
        &:after {
          transform: translateY(0) rotate(-45deg);
        }
      }
    `}

  @media ${({ theme }) => theme.media.tablet} {
    top: 32px;
    right: 5%;
    display: none;
  }
`

const HamburgerIcon = styled.span`
  background-color: ${({ theme }) => theme.colors.white};
  bottom: auto;
  display: inline-block;
  height: 3px;
  left: 50%;
  position: absolute;
  right: auto;
  top: 50%;
  width: 18px;
  z-index: 10;
  transform: translateX(-50%) translateY(-50%);

  &:before,
  &:after {
    background-color: ${({ theme }) => theme.colors.white};
    content: '';
    height: 100%;
    position: absolute;
    right: 0;
    top: 0;
    width: 100%;
    transform: translateZ(0);
    backface-visibility: hidden;
    transition: transform 0.3s;
  }

  &:before {
    transform: translateY(-6px) rotate(0deg);
  }

  &:after {
    transform: translateY(6px) rotate(0deg);
  }
`

// AppHeader.propTypes = {
//   siteTitle: PropTypes.string,
// }

// AppHeader.defaultProps = {
//   siteTitle: ``,
// }

const componentQuery = graphql`
  query {
    logoFile: file(relativePath: { eq: "logo.png" }) {
      publicURL
    }
  }
`

export default AppHeader
