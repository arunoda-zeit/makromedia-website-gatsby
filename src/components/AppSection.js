// import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const AppSection = ({ children }) => <Section>{children}</Section>

const Section = styled.section`
  width: 100%;
  padding: 0;
`

export default AppSection
