import React from 'react'
// import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import { Grid, Col, Row } from 'react-styled-flexboxgrid'
import JobCard from './JobCard'

const JobsSection = ({ props }) => {
  return (
    <StaticQuery
      query={componentQuery}
      render={data => (
        <StyledSection>
          {/* <AppContainer> */}
          <Grid fluid={true}>
            <Row>
              {data.allMarkdownRemark.edges
                .filter(jobOffer => jobOffer.node.frontmatter.isActive === true)
                .map(({ node: jobOffer }) => {
                  return (
                    <Col xs={12} md={6} key={jobOffer.id}>
                      <JobCard
                        title={jobOffer.frontmatter.title}
                        description={jobOffer.frontmatter.description}
                        path={jobOffer.fields.slug}
                      />
                    </Col>
                  )
                })}
            </Row>
          </Grid>
        </StyledSection>
      )}
    />
  )
}
const StyledSection = styled.section`
  margin: 40px auto;
`

const componentQuery = graphql`
  {
    allMarkdownRemark {
      totalCount
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            description
            isActive
          }
        }
      }
    }
  }
`

export default JobsSection
