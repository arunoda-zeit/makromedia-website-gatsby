import React from 'react'
// import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import styled from 'styled-components'
import { Col, Row } from 'react-styled-flexboxgrid'

const JobCard = props => (
  <Card to={props.path}>
    <Row>
      <Col xs={12}>
        <Title>{props.title}</Title>
      </Col>
      <Col xs={12}>
        <Description>{props.description}</Description>
      </Col>
    </Row>
  </Card>
)

const Title = styled.h4`
  font-weight: ${({ theme }) => theme.font.weight.bold};
  margin-top: 10px;
  padding-bottom: 10px;
  color: ${({ theme }) => theme.colors.nav};
  border-bottom: 1px solid ${({ theme }) => theme.colors.nav};
`

const Description = styled.p`
  color: ${({ theme }) => theme.colors.nav};
`

const Card = styled(Link)`
  display: block;
  border: 1px solid ${({ theme }) => theme.colors.nav};
  border-radius: 10px;
  height: 200px;
  max-width: 450px;
  transform: translateZ(0);
  transition: all 0.3s;
  margin: 0 auto 1rem;
  cursor: pointer;
  position: relative;
  padding: 10px 20px;
  overflow-y: hidden;

  @media ${({ theme }) => theme.media.tablet} {
    max-width: 100%;
  }

  &:hover {
    transform: translateY(-8px);
    box-shadow: rgba(0, 0, 0, 0.23) 0 0 12px;
    background-color: ${({ theme }) => theme.colors.nav + 'ee'};
    filter: none;

    & * {
      color: ${({ theme }) => theme.colors.white};
    }

    ${Title} {
      border-bottom: 1px solid ${({ theme }) => theme.colors.white};
    }
  }
`

export default JobCard
