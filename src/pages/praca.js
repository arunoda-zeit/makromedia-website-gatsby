import React from 'react'
// import { Link } from 'gatsby'

import Layout from '../components/Layout'
import SEO from '../components/seo'
import GlobalStyle from '../theme/GlobalStyle'
import AppPageTitle from '../components/AppPageTitle'
import AppContainer from '../components/AppContainer'
import JobsSection from '../components/Jobs/JobsSection'

const JobsPage = () => (
  <>
    <GlobalStyle />
    <Layout>
      <SEO title="Praca" />
      <AppContainer>
        <AppPageTitle title="Praca" />
        <JobsSection />
      </AppContainer>
    </Layout>
  </>
)

export default JobsPage
