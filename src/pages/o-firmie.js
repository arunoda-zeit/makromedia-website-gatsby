import React from 'react'
// import { Link } from 'gatsby'
import Layout from '../components/Layout'
import SEO from '../components/seo'
import GlobalStyle from '../theme/GlobalStyle'
import AppContainer from '../components/AppContainer'
import AppPageTitle from '../components/AppPageTitle'

const AboutCompanyPage = () => (
  <>
    <GlobalStyle />
    <Layout>
      <SEO title="O firmie" />
      <AppContainer>
        <AppPageTitle title="O firmie" />
      </AppContainer>
    </Layout>
  </>
)

export default AboutCompanyPage
