import React from 'react'
// import { Link } from 'gatsby'
import Layout from '../components/Layout'
import SEO from '../components/seo'
import GlobalStyle from '../theme/GlobalStyle'
import AppContainer from '../components/AppContainer'
import AppPageTitle from '../components/AppPageTitle'
import ContactSection from '../components/Contact/ContactSection'

const ContactPage = () => (
  <>
    <GlobalStyle />
    <Layout>
      <SEO title="Kontakt" />
      <AppContainer>
        <AppPageTitle title="Kontakt" />
        <ContactSection />
      </AppContainer>
    </Layout>
  </>
)

export default ContactPage
