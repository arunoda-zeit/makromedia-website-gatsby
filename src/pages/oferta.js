import React from 'react'
// import { Link } from 'gatsby'
import Layout from '../components/Layout'
import SEO from '../components/seo'
import GlobalStyle from '../theme/GlobalStyle'
import AppContainer from '../components/AppContainer'
import AppPageTitle from '../components/AppPageTitle'

const OfferPage = () => (
  <>
    <GlobalStyle />
    <Layout>
      <SEO title="Oferta" />
      <AppContainer>
        <AppPageTitle title="Oferta" />
      </AppContainer>
    </Layout>
  </>
)

export default OfferPage
