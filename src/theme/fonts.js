import 'typeface-raleway'
import 'typeface-open-sans'
import 'typeface-cardo'

import ElegantThemeLineEOT from '../fonts/elegant-theme-line.eot'
import ElegantThemeLineSVG from '../fonts/elegant-theme-line.svg'
import ElegantThemeLineTTF from '../fonts/elegant-theme-line.ttf'
import ElegantThemeLineWOFF from '../fonts/elegant-theme-line.woff'

import FlexSliderIconEOT from '../fonts/flexslider-icon.eot'
import FlexSliderIconSVG from '../fonts/flexslider-icon.svg'
import FlexSliderIconTTF from '../fonts/flexslider-icon.ttf'
import FlexSliderIconWOFF from '../fonts/flexslider-icon.woff'

import GlyphiconsHalflingsRegularEOT from '../fonts/glyphicons-halflings-regular.eot'
import GlyphiconsHalflingsRegularSVG from '../fonts/glyphicons-halflings-regular.svg'
import GlyphiconsHalflingsRegularTTF from '../fonts/glyphicons-halflings-regular.ttf'

export default {
  ElegantThemeLineEOT,
  ElegantThemeLineSVG,
  ElegantThemeLineTTF,
  ElegantThemeLineWOFF,

  FlexSliderIconEOT,
  FlexSliderIconSVG,
  FlexSliderIconTTF,
  FlexSliderIconWOFF,

  GlyphiconsHalflingsRegularEOT,
  GlyphiconsHalflingsRegularSVG,
  GlyphiconsHalflingsRegularTTF,
}
